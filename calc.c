#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ls.h"
#define MAX 5

char *BufferReceiver(){
    char *buffer;
    printf("Type bellow your expression:\n");
    printf("Exemple: ( ( 1 + 2 ) + ( 3 * 4 ) / ( 15 - 7 ) )\n");
    buffer = (char *)malloc(sizeof(char)*MAX);
    fgets(buffer, 10, stdin);

    if (1 <= 0){
        printf("Invalid Expression, try again:\n");
        free(buffer);
        return NULL;
    }

return buffer;
}

int Priority(char c, char t){
  int a,z;

  if(c == '^')
    a = 4;
  else if(c == '*' || c == '/')
    a = 2;
  else if(c == '+' || c == '-')
    a = 1;
  else if(c == '(')
    a = 4;

  if(t == '^')
    z = 3;
  else if(t == '*' || t == '/')
    z = 2;
  else if(t == '+' || t == '-')
    z = 1;
  else if(t == '(')
    z = 0;

  return (a > z);
}

void PrintDebug(struct IntoPosList *l){
    int i;
    for(i=0; i<l->tam; i++){
        printf("%c" , l->arm[i]);
    }
}

void In2Pos(struct IntoPosList *l){
    struct IntoPosList *Pilha = createC(l->tam);
    struct IntoPosList *Aux = createC(l->tam);

    int i = 0;
    int j = 0;
    char c,t;

    PushToPosList(Pilha, '(');
    do{
    c = l->arm[i];
    i++;

    if((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')){
      Aux->arm[j] = c;
      j++;
    }
    else if(c == '('){
      PushToPosList(Pilha, '(');
    }
    else if(c == ')' || c == '\0'){
      do{
        t = PopToPosList(Pilha);
        if(t != '('){
            Aux->arm[j] = t;
            j++;
           }

      }while(t != '(');
    }
    else if(c == '+' || c == '-' ||
            c == '*' || c == '/' ||
            c == '^' ){
      while(1){
        t = PopToPosList(Pilha);
        if(Priority(c,t)){
          PushToPosList(Pilha, t);
          PushToPosList(Pilha, c);
          break;
        }
        else{
        Aux->arm[j] = t;
        j++;
        }
      }
    }
    }while(c != '\0');

    Aux->tam = j+1;
    Aux->arm[j] = '\0';
    Aux->tam = strlen(Aux->arm);
    strcpy(l->arm, Aux->arm);
    l->tam = Aux->tam;
    free(Aux);
    free(Pilha);
    printf("\n");
}

/*Calculadora Lixo %$%@#$#@!!!*/
int caralho(void) {

    char line[1];
    int x, y;
    int i=0;
    char expr[] = "(2 + 3) * 4";

    struct IntoPosList * c = createC((int)strlen(expr));
    c->tam = (int)strlen(expr);
    strcpy(c->arm, expr);

    printf("\nEntrada:");
    PrintDebug(c);
    In2Pos(c);
    printf("\nSainda In2Pos:");
    PrintDebug(c);

    char expr2[100];
    strcpy(expr2, c->arm);
    strcat(expr2, "=q\0");

  do {
    line[1] = '\n';
    line[0] = expr2[i];
    i++;
    if(!strcmp(line, "+\n")){
        PushToInfList(PopToInfList() + PopToInfList());
    }

    else if(!strcmp(line, "-\n")) {
      x = PopToInfList();
      y = PopToInfList();
      PushToInfList(y - x);
    }
    else if(!strcmp(line, "*\n"))
      PushToInfList(PopToInfList() * PopToInfList());
    else if(!strcmp(line, "/\n")) {

      x = PopToInfList();
      y = PopToInfList();
      PushToInfList(y / x);
    }
    else if(!strcmp(line, "=\n")) {
      printf("\nResultado: %d\n", PeekToInfList());
    }
    else {
      sscanf(line, "%d", &x);
      PushToInfList(x);
    }

  } while(strcmp(line, "q\n"));

  return 0;
}
