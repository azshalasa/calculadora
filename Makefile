CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I.. 

.PHONY: ls le pilha all clean


all: test

ls.o: ls.c

# coloque outras dependencias aqui


test: ls.o test.c



clean:
	rm -f *.o test


