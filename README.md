# AED1 - Trabalho 1 - Calculadora

Declaro que o presente trabalho contém código desenvolvido exclusivamente por mim e que não foi compartilhado de nenhuma forma com terceiros a não ser o professor da disciplina. Compreendo que qualquer violação destes princípios será punida rigorosamente de acordo com o Regimento da UFPEL.

(Preencha com seus dados)

- Nome completo: Mateus Cordova da Silva Santos
- Username do Bitbucket: Azshalasa
- Email @inf: mcdssantos@inf.ufpel.edu.br

Disciplina: Algoritimos & Estrutura de Dados

## Descrição 

Este trabalho consiste em desenvolver uma calculadora de expressões contendo números inteiros usando notação polonesa reversa (ou pós-fixada). O cálculo consiste de três etapas: 

- Ler e validar a expressão
- Transformar para pós-fixada
- Calcular


## Entrada

A entrada consiste de uma _string_ com a expressão infixada contendo os seguintes caracteres além de dígitos decimais: 

    ( , ) , + , - , * , / , ^ , %

_^_ refere-se à exponenciação, _%_ ao resto da divisão.

A prioridade das operações em ordem decrescente é:

- ( , )
- ^
- *, /, %
- +, -

Não há operadores _-_ e _+_ unários.

Exemplo: 

    ( 5 + 4 ) % 4

É calculada como:

    ( 9 ) % 4
    1

Todas as operações, parênteses e números devem estar separados por exatamente um espaço. Qualquer expressão que não esteja em conformidade deve ser considerada inválida.

Para exemplos de conversão, vejam [2]. 

## Saída

A saída consiste de um inteiro e uma condição de erro que indica se:

- OVERFLOW
- UNDERFLOW
- MALFORMADA
- OK

## Algoritmo de conversão infixada para pós-fixada

    Entrada: fila E com expressão infixada
    Saída: fila S com expressão pós-fixada

    Pilha P

    Para cada elemento x retirado de E:
        Caso x for:
            operando: Coloque em S
            ( : coloque em P
            ) : tire operandores de S até encontrar (, adicionando-os a S
            operador: tire operadores de S enquanto topo de P tiver precedência maior ou igual a x ou (, coloque em S
                      coloque x em P

    Tire todos os operandos de P e coloque em S
        // Se pilha ainda conter ( , a expressão é inválida


## Algoritmo de cálculo

O algoritmo não considera _underflow_ e _overflow_, mas a implementação deve testar.


    Entrada: fila E com expressão pós-fixada
    Saída: inteiro

    Pilha P

    Para cada elemento x de E:
        Se x for:
            operando: coloque em P
            operador: retire dois operandos de P e realize operação, colocando resultado em P

    Retire topo da pilha para y:
        Se pilha vazia:
            Retorne y
        Se pilha não vazia:
            Retorne erro


## Produtos

- aluno.c: testes do aluno
- calc.c: implementação da calculadora

A versão final deverá executar os testes dos Professores no Bitbucket usando a regra _make test_. 

O Makefile poderá ser modificado para adicionar as regras para construir as bibliotecas de listas, filas e pilhas que serão copiadas do repositório do aluno. 



## Cronograma

- Entrega final: _11/05/2018_
- Período de correções: até _18/05/2018_


## Referências

- [1]https://pt.wikipedia.org/wiki/Nota%C3%A7%C3%A3o_polonesa_inversa
- [2]https://www.mathblog.dk/tools/infix-postfix-converter/

