#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ls.h"
/*Pos List Functions*/
struct IntoPosList * createC(int max){
    if(max <= 0){
        struct IntoPosList * l;
        l= NULL;
    return l;
    }
    else if(max >= 1 && max <= 20){
        struct IntoPosList * l = (struct IntoPosList*)malloc(sizeof(struct IntoPosList));
        l->tam = 0;
        l->max = max;
        l->arm = (char*)malloc(sizeof(char)*max);
    return l;
    }

    struct IntoPosList * l;
    l= NULL;
    return l;
};

int length(struct IntoPosList *desc){
    return desc->tam;
}

void destroy(struct IntoPosList *desc){
    free(desc->arm);
    free(desc);
}

int full(struct IntoPosList *desc){
    if(max(desc) == length(desc)){
        return 1;
    }
    return 0;
}

int max(struct IntoPosList *desc){
    return desc->max;
}

char PopToPosList(struct IntoPosList *desc){
    desc->tam = desc->tam-1;
    return desc->arm[desc->tam+1];
}

void PushToPosList(struct IntoPosList *desc, char valor){
    if(full(desc) == 0){
        desc->tam = desc->tam+1;
        desc->arm[desc->tam] = valor;
    }else{
    printf("Pilha Cheia");
    }
}

/*Inf List Funcions*/
int PushToInfList(int c) {
  if(IntoInfList.size == STACKSIZE)
    return 0;
  return IntoInfList.data[IntoInfList.size++] = c;
}

int PopToInfList() {
  if(IntoInfList.size == 0)
    return 0;
  return IntoInfList.data[--IntoInfList.size];
}

int PeekToInfList() {
  if(IntoInfList.size == 0)
    return 0;
  return IntoInfList.data[IntoInfList.size-1];
}
