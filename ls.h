typedef char elemC;
#define STACKSIZE 10

struct IntoPosList{
  int tam;
  int max;
  elemC * arm;
};

struct IntoInfList2 {
  int data[STACKSIZE];
  int size;
} IntoInfList;

struct IntoPosList * createC(int max);

int caralho(void);
void PushToPosList(struct IntoPosList  *desc, char valor);
char PopToPosList(struct IntoPosList  *desc);

void In2Pos(struct IntoPosList *l);
int Priority(char c, char t);
void PrintDebug(struct IntoPosList *l);

int length(struct IntoPosList *desc);
int max(struct IntoPosList *desc);
int full(struct IntoPosList *desc);
void destroy(struct IntoPosList *desc);

int PushToInfList(int c);
int PopToInfList();
int PeekToInfList();
